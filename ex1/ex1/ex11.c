// 201508793 Eran Haberman
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

#define BUFF_SIZE 1
#define BUFF_SIZE_FAIL 2
#define LETTER_CASE 32
#define FAIL_TEXT "-1"
typedef enum {DIFFERENT = 1, SAME, IDENTICAL, FAIL = -1} Result;


int main(int argc, char *argv[]) {
	int return_val = SAME;
	int firstFile, secondFile, firstEnd, secondEnd;
	char firstBuff, secondBuff, buff[BUFF_SIZE];

	// check if we didnt get the two files path
	if (argc < 3) {
		write(2, FAIL_TEXT, BUFF_SIZE_FAIL);
		exit(FAIL);
	}
	// opening the second files
	firstFile = open(argv[1], O_RDONLY);
	if (firstFile < 0) {/* means first file open did not take place */
		write(2, FAIL_TEXT, BUFF_SIZE_FAIL);
		exit(FAIL);
	}
	secondFile = open(argv[2], O_RDONLY);
	if (secondFile < 0) { /* means second file open did not take place */
		close(firstFile);
		write(2, FAIL_TEXT, BUFF_SIZE_FAIL);
		exit(FAIL);
	}

	// check if the file are same identical or different
	do {
		firstEnd = read(firstFile, buff, BUFF_SIZE);
		firstBuff = buff[0];
		secondEnd = read(secondFile, buff, BUFF_SIZE);
		secondBuff = buff[0];

		// deal with ' ' and '\n' in first file
		if (((secondEnd == 0) || (firstBuff != secondBuff)) &&
			((firstBuff == ' ') || (firstBuff == '\n'))) {
			return_val = IDENTICAL;
			do {
				firstEnd = read(firstFile, buff, BUFF_SIZE);
				firstBuff = buff[0];
			} while ((firstEnd == 1) && ((firstBuff == ' ') ||
					 (firstBuff == '\n')));
		}

		// deal with ' ' and '\n' in second file
		if (((firstEnd == 0) || (firstBuff != secondBuff)) &&
			((secondBuff == ' ') || (secondBuff == '\n'))) {
			return_val = IDENTICAL;
			do {
				secondEnd = read(secondFile, buff, BUFF_SIZE);
				secondBuff = buff[0];
			} while ((secondEnd == 1) && ((secondBuff == ' ') ||
					 (secondBuff == '\n')));
		}

		// check for same letter different case which mean identical not same
		if ((((firstBuff >= 'A') && (firstBuff <= 'Z')) ||
			 ((firstBuff >= 'a') && (firstBuff <= 'z'))) &&
			(((firstBuff - secondBuff) == LETTER_CASE) ||
			 ((firstBuff - secondBuff) == -LETTER_CASE)))
			return_val = IDENTICAL;
		// the letter is not alphabet letter which mean it have to be the same
		else if (firstBuff != secondBuff)
			break;
	} while ((firstEnd == BUFF_SIZE) && (secondEnd == BUFF_SIZE));

	// deal with extra letter in one file and different letters (while = break)
	if ((firstEnd != 0) || (secondEnd != 0))
		return_val = DIFFERENT;

	close(firstFile);
	close(secondFile);
	return return_val;
}