#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <sys/wait.h>

#define LINE_SIZE 161 // 160 per line + 1 for '\0'
#define BUFF_SIZE 1
#define BUFF_SIZE_FAIL 2
#define FAIL_TEXT "-1"
#define BAD_OUTPUT 1
#define GREAT_JOB 2
#define SIMILLAR_OUTPUT 3
#define NO_C_FILE 4
#define COMPILATION_ERROR 5
#define TIMEOUT 6
#define DEPTH_CALCULATE(x) (100-(10*x))


typedef enum {DIFFERENT = 1, SAME, IDENTICAL, FAIL = -1} Result;


void Get_Paths(char file_Path[], char path[],
	char input_Path[], char output_Path[]);


/******************************************************************************
* function name: Read_Line.                                                   *
* the Input: string to read the line into size 161, a buffer size 1 and the fd*
*			 of the file we reading.										  *
* the output: the number of letters that have been read from the file. -2 if  *
*			  the number of letters in the file greater then 160 letters	  *
* the Function operation: read a line from a file into string.				  *
******************************************************************************/
int Read_Line(char path[LINE_SIZE], char buff[BUFF_SIZE], int fdin);

void Walk_In_Dir(int first, char path[], char input_Path[],
	char output_Path[]);


int Find_C_Files(char path[], char input_Path[], char output_Path[],
	int *depth);


int Check_C(char path[]);


int File_Exist(char file_Name[]);


void Compile_C(char file_Name[], char comp_Name[]);


int Run_C(char run_Name[], char input_Path[], char student_output[]);


int Check_Output(char output_Path[], char student_output[]);


int Check_Compile(char file_Name[], char input_Path[], char output_Path[]);


void Give_Grade(int depth, int result);


int main(int argc, char *argv[]) {
	if (argc < 2) { // we dont get the configuration file path
		write(2, FAIL_TEXT, BUFF_SIZE_FAIL);
		exit(FAIL);
	} else {
		int resultfd;
		if ((resultfd = open("results.csv", O_CREAT|O_TRUNC|O_WRONLY, 0644)) <
		    0) {
			write(2, FAIL_TEXT, BUFF_SIZE_FAIL);
			exit(FAIL);
		}
		// get the paths to folders, input and output
		char path[LINE_SIZE], input_Path[LINE_SIZE], output_Path[LINE_SIZE];
		Get_Paths(argv[1], path, input_Path, output_Path);

		dup2(resultfd, 1); // write to the file
		// find the c files and check them
		Walk_In_Dir(1, path, input_Path, output_Path);
		return 0;
	}
}


/**
 * get the paths of the students folders and the right input and output
 */
void Get_Paths(char file_Path[], char path[],
	char input_Path[], char output_Path[]) {
	int fdin, is_End;
	char buff[BUFF_SIZE];

	// read the configuration file
	fdin = open(file_Path, O_RDONLY);
	if (fdin < 0) {/* means configuration file open did not take place */
		write(2, FAIL_TEXT, BUFF_SIZE_FAIL);
		exit(FAIL);
	}

	// read the path
	is_End = Read_Line(path, buff, fdin);
	if (is_End != 1) { //check if we succeeded in reading the line
		write(2, FAIL_TEXT, BUFF_SIZE_FAIL);
		exit(FAIL);
	}

	//read the input path
	is_End = Read_Line(input_Path, buff, fdin);
	if (is_End != 1) { //check if we succeeded in reading the line
		write(2, FAIL_TEXT, BUFF_SIZE_FAIL);
		exit(FAIL);
	}
	//read the output path
	is_End = Read_Line(output_Path, buff, fdin);
	if (is_End == -2) { //check if we succeeded in reading the line
		write(2, FAIL_TEXT, BUFF_SIZE_FAIL);
		exit(FAIL);
	}

	// end reading closing the file
	close(fdin);
	return;
}


/**
 * read 1 line from the argv[1] and make it a path
 */
int Read_Line(char path[LINE_SIZE], char buff[BUFF_SIZE], int fdin) {
	int is_End, i = 0;
	is_End = read(fdin, buff, BUFF_SIZE);
	do {
		if (i == 160) // we got more then we need end;
			return -2;
		path[i] = buff[0];
		i++;
		is_End = read(fdin, buff, BUFF_SIZE); //read 1 char from the file
	} while ((is_End == 1) && (buff[0] != '\n'));
	path[i] = '\0'; // end the string
	return is_End;
}


/**
 * the function go through the students folders
 */
void Walk_In_Dir(int first, char path[], char input_Path[],
	char output_Path[]) {
	DIR *pDir;
	struct dirent *pDirent;
	if ((pDir = opendir(path)) == NULL) {
		write(2, FAIL_TEXT, BUFF_SIZE_FAIL);
		exit(FAIL);
	}
	pDirent = readdir(pDir);
	pDirent = readdir(pDir);
	while ((pDirent = readdir(pDir)) != NULL) {
		int depth_For_Pointer, *depth = &depth_For_Pointer;
		int return_Val;
		*depth = 0;
		if (first) {
			write(1, pDirent->d_name, strlen(pDirent->d_name));
			first = 0;
		} else {
			write(1, "\n", strlen("\n"));
			write(1, pDirent->d_name, strlen(pDirent->d_name));
		}
		int len_New = strlen(pDirent->d_name);
		int i, len_Path = strlen(path);
		char new_Path[len_Path + len_New + 2];
		for (i = 0; i < len_Path; i++) {
			new_Path[i] = path[i];
		}
		new_Path[i] = '/';
		for (i = 0; i < len_New; i++) {
			new_Path[len_Path + i + 1] = pDirent->d_name[i];
		}
		new_Path[len_Path + 1 + i] = '\0';
		return_Val = Find_C_Files(new_Path, input_Path, output_Path, depth);
		Give_Grade(*depth, return_Val); // write the grade of the student
	}
	closedir(pDir);
	return;
}


/**
 *find c-file in specific student folder
 */
int Find_C_Files(char path[], char input_Path[], char output_Path[],
	int *depth) {
	DIR *pDir;
	struct dirent *pDirent;
	if ((pDir = opendir(path)) == NULL) {
		write(2, FAIL_TEXT, BUFF_SIZE_FAIL);
		exit(FAIL);
	}
	pDirent = readdir(pDir); // .
	pDirent = readdir(pDir); // ..
	while ((pDirent = readdir(pDir)) != NULL) { //while there are file
		// make the new path using the old path and the current file name
		int len_New = strlen(pDirent->d_name);
		int i, len_Path = strlen(path);
		char new_Path[len_Path + len_New + 2];
		for (i = 0; i < len_Path; i++) {
			new_Path[i] = path[i];
		}
		new_Path[i] = '/';
		for (i = 0; i < len_New; i++) {
			new_Path[len_Path + i + 1] = pDirent->d_name[i];
		}
		new_Path[len_Path + 1 + i] = '\0';

		if (!Check_C(new_Path)) { // check if the file is c-file
			int result;
			*depth += 1; // update the depth
			result = Find_C_Files(new_Path, input_Path, output_Path, depth);
			if (result != NO_C_FILE) //make sure we do not skip the c-file
				return result;
		} else {
			// compile the c file and check it against the right output
			return Check_Compile(new_Path, input_Path, output_Path);
		}
	}
	closedir(pDir);
	return NO_C_FILE;
}


/**
 * check if a given path is of c-file or not
 */
int Check_C(char path[]) {
	int len = strlen(path);
	if ((path[len - 1] == 'c')  &&
		(path[len - 2] == '.'))
		return 1;
	else
		return 0;
}


/**
 * the function use to check if the given file exist or not
 */
int File_Exist(char file_Name[]) {
	int result = 0;
	int fd;
	fd = open(file_Name, O_RDONLY);
	if (fd < 0)
		result = 0;
	else
		result = 1;
	close(fd);
	return result;
}


/**
 * the function compile a student c-file
 */
void Compile_C(char file_Name[], char comp_Name[]) {
	pid_t pid = fork();
	if (pid == 0 )
	{ /* child process */
		// compile the c file to comp_Name
		execlp("gcc", "gcc", file_Name, "-o", comp_Name, NULL);
	}
	else
	{ /* pid!=0; parent process */
		waitpid(pid, 0, 0); /* wait for child to exit */
	}
	return;
}


/**
 * the function get a compiled file and run it with the right input
 * the function send back the output of the run
 */
int Run_C(char run_Name[], char input_Path[], char student_output[]) {
	pid_t pid = fork();
	if (pid == 0) {
		int infd, outfd;

		if ((infd = open(input_Path, O_RDONLY)) < 0) //the input file
			write(2, FAIL_TEXT, BUFF_SIZE_FAIL);
		// the output file
		if ((outfd = open(student_output, O_CREAT|O_TRUNC|O_WRONLY, 0644)) < 0)
			write(2, FAIL_TEXT, BUFF_SIZE_FAIL);

		dup2(infd, 0);
		dup2(outfd, 1);
		execlp(run_Name, run_Name, NULL); //run the student file
	} else {
		// we check for timeout error
		char buff[2];
		int return_Val, check;
		sleep(5);
		if ((check = open(student_output, O_RDONLY)) < 0)
			write(2, FAIL_TEXT, BUFF_SIZE_FAIL);
		return_Val = read(check, buff, BUFF_SIZE);
		close(check);
		// if the file have context it mean that the child stop within 5 sec
		return return_Val;
	}
}


/**
 * check the output using the ex11.c (comp.out)
 */
int Check_Output(char output_Path[], char student_output[]) {
	pid_t pid = fork();
	if (pid == 0)
	{ /* child process */
		// compile the c file to comp_Name
		execlp("./comp.out", "./comp.out", output_Path, student_output, NULL);
	}
	else
	{ /* pid!=0; parent process */
		int status;
		waitpid(pid, &status, 0); /* wait for child to exit */
		return WEXITSTATUS(status);
	}
}


/**
 * the function get file and compile and check it with the right input and
 * output
 */
int Check_Compile(char file_Name[], char input_Path[], char output_Path[]) {
	int return_Val;
	char comp_Name[] = "student.out", run_Name[] = "./student.out";
	char student_output[] = "student_output.txt";

	// compile
	Compile_C(file_Name, comp_Name);
	// check if compile right (COMPILATION_ERROR) if not there is no .out file
	if (!File_Exist(comp_Name)) {
		unlink(comp_Name);
		return COMPILATION_ERROR;
	}

	// run
	if (!Run_C(run_Name, input_Path, student_output)) {
		unlink(comp_Name);
		unlink(student_output);
		return TIMEOUT;
	}

	// check the output file against the correct output
	return_Val = Check_Output(output_Path, student_output);

	//delete the files
	unlink(comp_Name);
	unlink(student_output);
	return return_Val;
}


/**
 *the function take the depth and the result of the run and write in
 *results.csv
 */
void Give_Grade(int depth, int result) {
	if ((result == GREAT_JOB) && (depth == 0)) {
		write(1, ",100,GREAT_JOB", strlen(",100,GREAT_JOB"));
	} else if (result == GREAT_JOB) {
		int new_Result = DEPTH_CALCULATE(depth);
		if (new_Result <= 0)
			write(1, ",0,WRONG_DIRECTORY", strlen(",0,WRONG_DIRECTORY"));
		else { // 100 < new_Result < 0
			char first_Digit[1], second_Digit[1];
			first_Digit[0] = (new_Result / 10) + '0';
			second_Digit[0] = (new_Result % 10) + '0';
			write(1, ",", strlen(","));
			write(1, first_Digit, 1);
			write(1, second_Digit, 1);
			write(1, ",WRONG_DIRECTORY", strlen(",WRONG_DIRECTORY"));
		}
	} else {
		switch (result) {
			case BAD_OUTPUT: write(1, ",0,BAD_OUTPUT", strlen(",0,BAD_OUTPUT"));
				break;
			case SIMILLAR_OUTPUT: write(1, ",80,SIMILLAR_OUTPUT",
				strlen(",80,SIMILLAR_OUTPUT"));
				break;
			case NO_C_FILE: write(1, ",0,NO_C_FILE", strlen(",0,NO_C_FILE"));
				break;
			case COMPILATION_ERROR: write(1, ",0,COMPILATION_ERROR",
				strlen(",0,COMPILATION_ERROR"));
				break;
			default: write(1, ",0,TIMEOUT", strlen(",0,TIMEOUT"));
		}
	}
	return;
}