/******************************************
* Eran haberman
* 8923101
* ex3 - ex31.c
******************************************/
#include <unistd.h>
#include <stdlib.h>
#include <sys/fcntl.h>
#include <signal.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/shm.h>

#define FIFONAME "fifo_clientTOserver"
#define BOARD_SIZE 8
#define SHM_SIZE 4 // the shared memory size

// print messages
#define END_MES "GAME OVER\n"
#define BLACK_WIN_MES "Winning player: Black\n"
#define WHITE_WIN_MES "Winning player: White\n"
#define EVEN_MES "No winning player\n"

typedef struct {
	char board[BOARD_SIZE][BOARD_SIZE];
	int pieces_Amount; // the amount of pieces on the board
} Game_Assets;


/******************************************************************************
* function name: Create_Board.                                                *
* the Input: no input.        		                                     	  *
* the output: a Game_Assets pointer.                                          *
* the Function operation: the function create a Game_Assets set it board to   *
*						  initial state of the game and set the counter to 4. *
******************************************************************************/
Game_Assets* Create_Board();


/******************************************************************************
* function name: Change_Add.                                                  *
* the Input: a Game_Assets pointer, int row, int column, two ints with the    *
			 movement in the row and column and the piece we check.	  		  *
* the output: 0 if there is this piece to block, 1 otherwise.				  *
* the Function operation: the function check if there is this piece in our way*
*						  so we will "eat" the other player piece if so we    *
*						  return 1 and change the piece recursively else just *
*						  return 0.											  *
******************************************************************************/
int Change_Add(Game_Assets *player, int row, int column, int add_Row,
		   int add_Column, char this_Piece);


/******************************************************************************
* function name: Change_Check.                                                *
* the Input: a Game_Assets pointer, int row, int column, two ints with the    *
			 movement in the row and column and the piece we check.	  		  *
* the output: 0 if there is empty spot, 1 otherwise.						  *
* the Function operation: the function check if there is empty spot to place  *
*						  our piece while "eating" the other player pieces.	  *
******************************************************************************/
int Change_Check(Game_Assets *player, int row, int column, int add_Row,
		   int add_Column, char this_Piece);


/******************************************************************************
* function name: Check_New_Piece.                                             *
* the Input: a Game_Assets pointer, int row, int column, the piece we check   *
*			 and its rival as char and a function to check directions with.	  *
* the output: 0 if the place is invalid, 1 otherwise.						  *
* the Function operation: the function check if when we put the piece in the  *
*						  given place we "eat" the other player piece if so   *
*						  return 1, else 0.									  *
******************************************************************************/
int Check_New_Piece(Game_Assets *player, int row, int column, char this_Piece,
					char other_Piece, int (*Add)(Game_Assets*, int, int, int,
					int, char));


/******************************************************************************
* function name: Can_Continue.                                                *
* the Input: a Game_Assets pointer, the request and rival pieces as chars.	  *
* the output: 1 if we can continue(there a place for new piece, 0 otherwise.  *
* the Function operation: the function check if we can place a new piece on   *
*						  the board.										  *
******************************************************************************/
int Can_Continue(Game_Assets *player, char this_Piece, char other_Piece);


/******************************************************************************
* function name: End_Game_Mes.                                                *
* the Input: a Game_Assets pointer.                                     	  *
* the output: no output.                                                      *
* the Function operation: the function print the winner or tie				  *
******************************************************************************/
void End_Game_Mes(Game_Assets *player);


int main(int argc, char *argv[]) {
	// initial the game
	Game_Assets *game;
	char cur_Player; // contain the color of the current player
	char next_Player; // contain the color of the next player
	char temp; // foe changing the players

	// initial the fifo
	pid_t white_Pid, black_Pid;
	int fd_Fifo; // the fd of the fifo
	int check; // indicates if we read the second pid and not garbage

	// initial the shared memory
	key_t key;
	int shmid;
	char *memory;

	//////////////////////////////////
	// create the shared memory
	//////////////////////////////////

	// create the  key
	if ((key = ftok("ex31.c", 'k')) == -1) {
		write(2, "ftok had a problem\n", strlen("ftok had a problem\n"));
		exit(-1);
	}

	// create the memory
	if ((shmid = shmget(key, SHM_SIZE, 0666 | IPC_CREAT)) == -1) {
		write(2, "shmget had a problem crearing the memory\n",
			  strlen("shmget had a problem crearing the memory\n"));
		exit(-1);
	}

	// attach to the segment to get a pointer to it
	memory = shmat(shmid, NULL, 0);
	if (memory == (char *)(-1)) {
		write(2, "shmat had a problem attaching to the memory\n",
			  strlen("shmat had a problem attaching to the memory\n"));
		exit(-1);
	}

	//////////////////////////////////
	// create the fifo
	//////////////////////////////////

	if (mkfifo(FIFONAME, 0666) < 0) {
		write(2, "Unable to create a fifo\n",
			  strlen("Unable to create a fifo\n"));
		exit(-1);
	}

	// get the two pid_t
	fd_Fifo = open(FIFONAME, O_RDONLY);
	if (fd_Fifo < 0) {
		write(2, "Error there is no fifo\n",
			  strlen("Error there is no fifo\n"));
		exit(-1);
	} else {
		read(fd_Fifo, &black_Pid, sizeof(int));
		check = read(fd_Fifo, &white_Pid, sizeof(int));
		while (check < 1) { // make sure we dont get garbage as result
			sleep(1);
			check = read(fd_Fifo, &white_Pid, sizeof(int));
		}
	}
	close(fd_Fifo);

	// delete the fifo
	unlink(FIFONAME);

	//////////////////////////////////
	// main logic
	//////////////////////////////////

	// start the main logic
	game = Create_Board();

	// start the first player
	kill(black_Pid, SIGUSR1);
	// make sure the first player make its play
	while(memory[0] != 'b') {
		sleep(1);
	}
	Check_New_Piece(game, ((int) (memory[2] - '1')), ((int) (memory[1] - '1')),
					'b', 'w', Change_Add);
	game->board[((int) (memory[2] - '1'))][((int) (memory[1] - '1'))] = 'b';
	game->pieces_Amount++;

	// start the second player
	kill(white_Pid, SIGUSR1);
	// make sure the second player make its play
	while(memory[0] != 'w') {
		sleep(1);
	}
	Check_New_Piece(game, ((int) (memory[2] - '1')), ((int) (memory[1] - '1')),
					'w', 'b', Change_Add);
	game->board[((int) (memory[2] - '1'))][((int) (memory[1] - '1'))] = 'w';
	game->pieces_Amount++;
	cur_Player = 'b';
	next_Player = 'w';

	// the game logic
	while(Can_Continue(game, cur_Player, next_Player)) {
		// wait for the current player to write to the shared memory
		while(memory[0] != cur_Player) {
			sleep(1);
		}
		Check_New_Piece(game, ((int) (memory[2] - '1')), ((int) (memory[1] -
						'1')), cur_Player, next_Player, Change_Add);
		game->board[((int) (memory[2] - '1'))][((int) (memory[1] - '1'))] =
		cur_Player;
		game->pieces_Amount++;

		// switch the players
		temp = cur_Player;
		cur_Player = next_Player;
		next_Player = temp;
	}

	//////////////////////////////////
	// end the game
	//////////////////////////////////

	write(1, END_MES, strlen(END_MES));
	End_Game_Mes(game);
	free(game);

	//////////////////////////////////
	// delete the shared memory
	//////////////////////////////////

	// detach from the segment
	if (shmdt(memory) == -1) {
		write(2, "shmdt had a problem detaching to the memory\n",
			  strlen("shmdt had a problem detaching to the memory\n"));
		exit(-1);
	}

	// remove the memory
	if (shmctl(shmid, IPC_RMID, NULL) == -1) {
		write(2, "shmctl had a problem removing the memory\n",
			  strlen("shmctl had a problem removing the memory\n"));
		exit(-1);
	}

	return 0;
}


/******************************************************************************
* function name: Create_Board.                                                *
* the Input: no input.        		                                     	  *
* the output: a Game_Assets pointer.                                          *
* the Function operation: the function create a Game_Assets set it board to   *
*						  initial state of the game and set the counter to 4. *
******************************************************************************/
Game_Assets* Create_Board() {
	// create Game_Assets
	Game_Assets *player = (Game_Assets*) malloc(sizeof(Game_Assets));
	int i,j;
	// start the board with empty spaces
	for (i = 0; i < BOARD_SIZE; i++) {
		for (j = 0; j< BOARD_SIZE; j++) {
			player->board[i][j] = '-';
		}
	}

	// set the initial pieces
	player->board[3][3] = 'w';
	player->board[4][4] = 'w';
	player->board[3][4] = 'b';
	player->board[4][3] = 'b';
	player->pieces_Amount = 4;

	return player;
}


/******************************************************************************
* function name: Change_Add.                                                  *
* the Input: a Game_Assets pointer, int row, int column, two ints with the    *
			 movement in the row and column and the piece we check.	  		  *
* the output: 0 if there is this piece to block, 1 otherwise.				  *
* the Function operation: the function check if there is this piece in our way*
*						  so we will "eat" the other player piece if so we    *
*						  return 1 and change the piece recursively else just *
*						  return 0.											  *
******************************************************************************/
int Change_Add(Game_Assets *player, int row, int column, int add_Row,
		   int add_Column, char this_Piece) {
	// check if we not in the board and if there is empty place
	if (((row + add_Row) < 0) || ((row + add_Row) >= BOARD_SIZE) ||
		((column + add_Column) < 0) || ((column + add_Column) >= BOARD_SIZE) ||
		(player->board[row + add_Row][column + add_Column] == '-'))
		return 0;

	else if (player->board[row + add_Row][column + add_Column] == this_Piece)
		return 1;

	else { // the piece is the other piece
		int check = Change_Add(player, row + add_Row, column + add_Column,
							   add_Row, add_Column, this_Piece);
		if (check) {
			player->board[row + add_Row][column + add_Column] = this_Piece;
		}
		return check;
	}
}


/******************************************************************************
* function name: Change_Check.                                                *
* the Input: a Game_Assets pointer, int row, int column, two ints with the    *
			 movement in the row and column and the piece we check.	  		  *
* the output: 0 if there is empty spot, 1 otherwise.						  *
* the Function operation: the function check if there is empty spot to place  *
*						  our piece while "eating" the other player pieces.	  *
******************************************************************************/
int Change_Check(Game_Assets *player, int row, int column, int add_Row,
		   int add_Column, char this_Piece) {
	// check if we not in the board and if there is this piece that block
	if (((row + add_Row) < 0) || ((row + add_Row) >= BOARD_SIZE) ||
		((column + add_Column) < 0) || ((column + add_Column) >= BOARD_SIZE) ||
		(player->board[row + add_Row][column + add_Column] == this_Piece))
		return 0;

	// check if we have empty space to put our piece in
	else if (player->board[row + add_Row][column + add_Column] == '-')
		return 1;

	else { // the piece is the other piece
		return Change_Check(player, row + add_Row, column + add_Column,
							   add_Row, add_Column, this_Piece);
	}
}


/******************************************************************************
* function name: Check_New_Piece.                                             *
* the Input: a Game_Assets pointer, int row, int column, the piece we check   *
*			 and its rival as char and a function to check directions with.	  *
* the output: 0 if the place is invalid, 1 otherwise.						  *
* the Function operation: the function check if when we put the piece in the  *
*						  given place we "eat" the other player piece if so   *
*						  return 1, else 0.									  *
******************************************************************************/
int Check_New_Piece(Game_Assets *player, int row, int column, char this_Piece,
					char other_Piece, int (*Add)(Game_Assets*, int, int, int,
					int, char)) {
	int i, j, check = 0;
	for (i = -1; i < 2; i++) {
		for (j = -1; j < 2; j++) {
			/**
			 * we check for three things
			 * 1) check that adding i and j is still in the board limit
			 * 2) check if the place contain rival piece
			 * 3) check if we changed any rival pieces
			 * note that i == 0, j == j will fail in the second check
			 */
			if (((row + i) >= 0) && ((row + i) < BOARD_SIZE) &&
				((column + j) >= 0) && ((column + j) < BOARD_SIZE) &&
				(player->board[row + i][column + j] == other_Piece) &&
				Add(player, row, column, i, j, this_Piece))
				check = 1;
		}
	}
	return check;
}


/******************************************************************************
* function name: Can_Continue.                                                *
* the Input: a Game_Assets pointer, the request and rival pieces as chars.	  *
* the output: 1 if we can continue(there a place for new piece, 0 otherwise.  *
* the Function operation: the function check if we can place a new piece on   *
*						  the board.										  *
******************************************************************************/
int Can_Continue(Game_Assets *player, char this_Piece, char other_Piece) {
	// check if the board is full
	if (player->pieces_Amount == (BOARD_SIZE * BOARD_SIZE))
		return 0;
	else {
		/**
		 * check if there a empty place we can put the request piece so it eat
		 * an other player piece (note we only checking not changing)
		 */
		int i, j;
		for (i = 0; i < BOARD_SIZE; i++) {
			for (j = 0; j < BOARD_SIZE; j++) {
				if ((player->board[i][j] == this_Piece) &&
					(Check_New_Piece(player, i, j, this_Piece,
									 other_Piece, Change_Check)))
					return 1;
			}
		}
		return 0;
	}
}


/******************************************************************************
* function name: End_Game_Mes.                                                *
* the Input: a Game_Assets pointer.                                     	  *
* the output: no output.                                                      *
* the Function operation: the function print the winner or tie				  *
******************************************************************************/
void End_Game_Mes(Game_Assets *player) {
	/**
	 * note that if the game stop in the middle their will be blank space so
	 * we need to check both white and black
	 */
	int i, j, whites = 0, blacks = 0;
	for (i = 0; i < BOARD_SIZE; i++) {
		for (j = 0; j < BOARD_SIZE; j++) {
			if (player->board[i][j] == 'w')
				whites++;
			else if (player->board[i][j] == 'b')
				blacks++;
		}
	}

	// print the result
	if (whites > blacks)
		write(1, WHITE_WIN_MES, strlen(WHITE_WIN_MES));
	else if (whites < blacks)
		write(1, BLACK_WIN_MES, strlen(BLACK_WIN_MES));
	else
		write(1, EVEN_MES, strlen(EVEN_MES));
}