/******************************************
* Eran haberman
* 8923101
* ex3 - ex32.c
******************************************/
#include <unistd.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/fcntl.h>
#include <sys/shm.h>

#define BOARD_SIZE 8
#define SHM_SIZE 4 // the shared memory size

// print messages
#define CHOOSE_MES "Please choose a square\n"
#define NOT_FOUND_MES "No such square\n"
#define INVALID_MES "This square is invalid\n"
#define BOARD_MES "The board is:\n"
#define WAIT_MES "Waiting for the other player to make a move\n"
#define BLACK_WIN_MES "Winning player: Black\n"
#define WHITE_WIN_MES "Winning player: White\n"
#define EVEN_MES "No winning player\n"

typedef struct {
	char board[BOARD_SIZE][BOARD_SIZE];
	char my_Piece; // the player color
	char rival_Piece; // the rival color
	int pieces_Amount; // the amount of pieces on the board
} Game_Assets;

int to_Start = 0; // indicate if we need to stop the program


/******************************************************************************
* function name: Start_Handle.                                                *
* the Input: the signal that raise the function.                              *
* the output: no output.                                                      *
* the Function operation: the function change the to_Start argument to 1 thus *
*						  signal the program to start.						  *
******************************************************************************/
void Start_Handle(int sig);


/******************************************************************************
* function name: Create_Board.                                                *
* the Input: no input.        		                                     	  *
* the output: a Game_Assets pointer.                                          *
* the Function operation: the function create a Game_Assets set it board to   *
*						  initial state of the game and set the counter to 4. *
******************************************************************************/
Game_Assets* Create_Board();


/******************************************************************************
* function name: Draw_Border.                                                 *
* the Input: no input.        		                                     	  *
* the output: no output.                                                      *
* the Function operation: the function draw a  border.     					  *
******************************************************************************/
void Draw_Border();


/******************************************************************************
* function name: Draw_Board.                                                  *
* the Input: a Game_Assets pointer.                                     	  *
* the output: no output.                                                      *
* the Function operation: the function draw the board in graphical shape.     *
******************************************************************************/
void Draw_Board(Game_Assets *player);


/******************************************************************************
* function name: Change_Add.                                                  *
* the Input: a Game_Assets pointer, int row, int column, two ints with the    *
			 movement in the row and column and the piece we check.	  		  *
* the output: 0 if there is this piece to block, 1 otherwise.				  *
* the Function operation: the function check if there is this piece in our way*
*						  so we will "eat" the other player piece if so we    *
*						  return 1 and change the piece recursively else just *
*						  return 0.											  *
******************************************************************************/
int Change_Add(Game_Assets *player, int row, int column, int add_Row,
		   int add_Column, char this_Piece);


/******************************************************************************
* function name: Change_Check.                                                *
* the Input: a Game_Assets pointer, int row, int column, two ints with the    *
			 movement in the row and column and the piece we check.	  		  *
* the output: 0 if there is empty spot, 1 otherwise.						  *
* the Function operation: the function check if there is empty spot to place  *
*						  our piece while "eating" the other player pieces.	  *
******************************************************************************/
int Change_Check(Game_Assets *player, int row, int column, int add_Row,
		   int add_Column, char this_Piece);


/******************************************************************************
* function name: Check_New_Piece.                                             *
* the Input: a Game_Assets pointer, int row, int column, the piece we check   *
*			 and its rival as char and a function to check directions with.	  *
* the output: 0 if the place is invalid, 1 otherwise.						  *
* the Function operation: the function check if when we put the piece in the  *
*						  given place we "eat" the other player piece if so   *
*						  return 1, else 0.									  *
******************************************************************************/
int Check_New_Piece(Game_Assets *player, int row, int column, char this_Piece,
					char other_Piece, int (*Add)(Game_Assets*, int, int, int,
					int, char));


/******************************************************************************
* function name: Get_New_Piece.                                               *
* the Input: a Game_Assets pointer, char pointer to the shared memory.        *
* the output: no output.                                                      *
* the Function operation: the function get place from the player check if it  *
*						  a place that valid for new piece and add it to the  *
*						  board while update the counts of the pieces and send*
*						  the info to the shared memory.					  *
******************************************************************************/
void Get_New_Piece(Game_Assets *player, char *memory);


/******************************************************************************
* function name: Can_Continue.                                                *
* the Input: a Game_Assets pointer, the request and rival pieces as chars.	  *
* the output: 1 if we can continue(there a place for new piece, 0 otherwise.  *
* the Function operation: the function check if we can place a new piece on   *
*						  the board.										  *
******************************************************************************/
int Can_Continue(Game_Assets *player, char this_Piece, char other_Piece);


/******************************************************************************
* function name: End_Game_Mes.                                                *
* the Input: a Game_Assets pointer.                                     	  *
* the output: no output.                                                      *
* the Function operation: the function print the winner or tie				  *
******************************************************************************/
void End_Game_Mes(Game_Assets *player);


int main(int argc, char *argv[]) {
	Game_Assets *player;
	int fd_Fifo;

	// initial the shared memory
	key_t key;
	int shmid;
	char *memory;

	/////////////////////////////////////////
	// make the signal handler
	/////////////////////////////////////////

	// make the sigaction
	struct sigaction sig_Act;
	sigset_t block_Mask;
	// block all signal
	sigfillset(&block_Mask);
	sig_Act.sa_mask = block_Mask;
	sig_Act.sa_flags = 0;

	//make the print/SIGUSR1 signal
	sig_Act.sa_handler = Start_Handle;
	if (sigaction(SIGUSR1, &sig_Act, NULL) != 0) {
		write(2, "Error did not create signal",
			  strlen("Error did not create signal"));
		exit(-1);
	}

	/////////////////////////////////////////
	// connect to the fifo
	/////////////////////////////////////////

	// open the fifo and send the program pid
	fd_Fifo = open("fifo_clientTOserver", O_WRONLY);
	if (fd_Fifo < 0) {
		write(2, "Error there is no FIFO", strlen("Error there is no fifo"));
		exit(-1);
	} else {
		pid_t this_Pid = getpid();
		write(fd_Fifo, &this_Pid, sizeof(int));
	}
	close(fd_Fifo);

	/////////////////////////////////////////
	// get and attach to the shared memory
	/////////////////////////////////////////

	// create key
	if ((key = ftok("ex31.c", 'k')) == -1) {
		write(2, "ftok had a problem", strlen("ftok had a problem"));
		exit(-1);
	}

	// create memory
	if ((shmid = shmget(key, SHM_SIZE, 0)) == -1) {
		write(2, "shmget had a problem crearing the memory",
			  strlen("shmget had a problem crearing the memory"));
		exit(-1);
	}

	// attach to the segment to get a pointer to it
	memory = shmat(shmid, NULL, 0);
	if (memory == (char *)(-1)) {
		write(2, "shmat had a problem attaching to the memory",
			  strlen("shmat had a problem attaching to the memory"));
		exit(-1);
	}

	/////////////////////////////////////////
	// main logic
	/////////////////////////////////////////

	player = Create_Board();
	Draw_Board(player);
	write(1, WAIT_MES, strlen(WAIT_MES));
	// start the program after we get SIGUSR1 signal
	while(!to_Start);

	//check if we black or white
	if (memory[0] != 'b'){
		player->my_Piece = 'b';
		player->rival_Piece = 'w';
	} else {
		player->my_Piece = 'w';
		player->rival_Piece = 'b';
		Check_New_Piece(player, ((int) (memory[2] - '1')), ((int) (memory[1] -
						'1')), player->rival_Piece, player->my_Piece,
						Change_Add);
		player->board[((int) (memory[2]-'1'))][((int) (memory[1]-'1'))] = 'b';
		player->pieces_Amount++;
		Draw_Board(player);
	}

	// start the game
	while(Can_Continue(player, player->my_Piece, player->rival_Piece)) {
		// get the player piece
		Get_New_Piece(player, memory);
		Draw_Board(player);

		// get the rival piece
		if (Can_Continue(player, player->rival_Piece, player->my_Piece)) {
			write(1, WAIT_MES, strlen(WAIT_MES));
			// wait for the rival player to write to the shared memory
			while (memory[0] == player->my_Piece) {
				sleep(1);
			}
			Check_New_Piece(player, ((int) (memory[2] - '1')),
							((int) (memory[1] - '1')), player->rival_Piece,
							player->my_Piece, Change_Add);
			player->board[((int) (memory[2] - '1'))][((int) (memory[1] - '1'))]
			= player->rival_Piece;
			player->pieces_Amount++;
			Draw_Board(player);
		} else {
			break;
		}
	}

	//////////////////////////////////
	// end the game
	//////////////////////////////////

	End_Game_Mes(player);
	free(player);

	//////////////////////////////////
	// detach from the segment
	//////////////////////////////////

	if (shmdt(memory) == -1) {
		write(2, "shmdt had a problem detaching to the memory",
			  strlen("shmdt had a problem detaching to the memory"));
		exit(-1);
	}

	return 0;
}


/******************************************************************************
* function name: Start_Handle.                                                *
* the Input: the signal that raise the function.                              *
* the output: no output.                                                      *
* the Function operation: the function change the to_Start argument to 1 thus *
*						  signal the program to start.						  *
******************************************************************************/
void Start_Handle(int sig) {
	to_Start = 1;
}


/******************************************************************************
* function name: Create_Board.                                                *
* the Input: no input.        		                                     	  *
* the output: a Game_Assets pointer.                                          *
* the Function operation: the function create a Game_Assets set it board to   *
*						  initial state of the game and set the counter to 4. *
******************************************************************************/
Game_Assets* Create_Board() {
	// create Game_Assets
	Game_Assets *player = (Game_Assets*) malloc(sizeof(Game_Assets));
	int i,j;
	// start the board with empty spaces
	for (i = 0; i < BOARD_SIZE; i++) {
		for (j = 0; j< BOARD_SIZE; j++) {
			player->board[i][j] = '-';
		}
	}

	// set the initial pieces
	player->board[3][3] = 'w';
	player->board[4][4] = 'w';
	player->board[3][4] = 'b';
	player->board[4][3] = 'b';
	player->pieces_Amount = 4;

	return player;
}


/******************************************************************************
* function name: Draw_Border.                                                 *
* the Input: no input.        		                                     	  *
* the output: no output.                                                      *
* the Function operation: the function draw a  border.     					  *
******************************************************************************/
void Draw_Border() {
	int i;
	for (i = 0; i < BOARD_SIZE; i++) {
		write(1, "____", 4);
	}
	write(1, "_\n", 2);
}


/******************************************************************************
* function name: Draw_Board.                                                  *
* the Input: a Game_Assets pointer.                                     	  *
* the output: no output.                                                      *
* the Function operation: the function draw the board in graphical shape.     *
******************************************************************************/
void Draw_Board(Game_Assets *player) {
	int i, j;
	write(1, BOARD_MES, strlen(BOARD_MES));
	Draw_Border(); // draw the top border
	for (i = 0; i < BOARD_SIZE; i++) {
		for (j = 0; j < BOARD_SIZE; j++) {
			write(1,"| ",2); // begin a line of pieces
			// draw the pieces on the board
			switch (player->board[i][j]) {
				case 'w': write(1, "W ", 2);
					break;
				case 'b': write(1, "B ", 2);
					break;
				default : write(1, "  ", 2);
			}
		}
		write(1,"|\n",2); // end the line of pieces
		Draw_Border(); // draw the bottom borders
	}
	write(1, "\n", 1);
}


/******************************************************************************
* function name: Change_Add.                                                  *
* the Input: a Game_Assets pointer, int row, int column, two ints with the    *
			 movement in the row and column and the piece we check.	  		  *
* the output: 0 if there is this piece to block, 1 otherwise.				  *
* the Function operation: the function check if there is this piece in our way*
*						  so we will "eat" the other player piece if so we    *
*						  return 1 and change the piece recursively else just *
*						  return 0.											  *
******************************************************************************/
int Change_Add(Game_Assets *player, int row, int column, int add_Row,
		   int add_Column, char this_Piece) {
	// check if we not in the board and if there is empty place
	if (((row + add_Row) < 0) || ((row + add_Row) >= BOARD_SIZE) ||
		((column + add_Column) < 0) || ((column + add_Column) >= BOARD_SIZE) ||
		(player->board[row + add_Row][column + add_Column] == '-'))
		return 0;

	else if (player->board[row + add_Row][column + add_Column] == this_Piece)
		return 1;

	else { // the piece is the other piece
		int check = Change_Add(player, row + add_Row, column + add_Column,
							   add_Row, add_Column, this_Piece);
		if (check) {
			player->board[row + add_Row][column + add_Column] = this_Piece;
		}
		return check;
	}
}


/******************************************************************************
* function name: Change_Check.                                                *
* the Input: a Game_Assets pointer, int row, int column, two ints with the    *
			 movement in the row and column and the piece we check.	  		  *
* the output: 0 if there is empty spot, 1 otherwise.						  *
* the Function operation: the function check if there is empty spot to place  *
*						  our piece while "eating" the other player pieces.	  *
******************************************************************************/
int Change_Check(Game_Assets *player, int row, int column, int add_Row,
		   int add_Column, char this_Piece) {
	// check if we not in the board and if there is this piece that block
	if (((row + add_Row) < 0) || ((row + add_Row) >= BOARD_SIZE) ||
		((column + add_Column) < 0) || ((column + add_Column) >= BOARD_SIZE) ||
		(player->board[row + add_Row][column + add_Column] == this_Piece))
		return 0;

	// check if we have empty space to put our piece in
	else if (player->board[row + add_Row][column + add_Column] == '-')
		return 1;

	else { // the piece is the other piece
		return Change_Check(player, row + add_Row, column + add_Column,
							   add_Row, add_Column, this_Piece);
	}
}


/******************************************************************************
* function name: Check_New_Piece.                                             *
* the Input: a Game_Assets pointer, int row, int column, the piece we check   *
*			 and its rival as char and a function to check directions with.	  *
* the output: 0 if the place is invalid, 1 otherwise.						  *
* the Function operation: the function check if when we put the piece in the  *
*						  given place we "eat" the other player piece if so   *
*						  return 1, else 0.									  *
******************************************************************************/
int Check_New_Piece(Game_Assets *player, int row, int column, char this_Piece,
					char other_Piece, int (*Add)(Game_Assets*, int, int, int,
					int, char)) {
	int i, j, check = 0;
	for (i = -1; i < 2; i++) {
		for (j = -1; j < 2; j++) {
			/**
			 * we check for three things
			 * 1) check that adding i and j is still in the board limit
			 * 2) check if the place contain rival piece
			 * 3) check if we changed any rival pieces
			 * note that i == 0, j == j will fail in the second check
			 */
			if (((row + i) >= 0) && ((row + i) < BOARD_SIZE) &&
				((column + j) >= 0) && ((column + j) < BOARD_SIZE) &&
				(player->board[row + i][column + j] == other_Piece) &&
				Add(player, row, column, i, j, this_Piece))
				check = 1;
		}
	}
	return check;
}


/******************************************************************************
* function name: Get_New_Piece.                                               *
* the Input: a Game_Assets pointer, char pointer to the shared memory.        *
* the output: no output.                                                      *
* the Function operation: the function get place from the player check if it  *
*						  a place that valid for new piece and add it to the  *
*						  board while update the counts of the pieces and send*
*						  the info to the shared memory.					  *
******************************************************************************/
void Get_New_Piece(Game_Assets *player, char *memory) {
	char buff[6]; // 6 == "[x,y]\n"
	int check = 1; // make sure we get place we can put the new piece
	char output[4];
	int row, column;

	while (check) {
		check = 0;
		// get the new piece from the user
		write(1, CHOOSE_MES, strlen(CHOOSE_MES));
		read(0, buff, 6);
		// convert to ints
		row = ((int)(buff[3] - '1'));
		column = ((int)(buff[1] - '1'));

		// deal with getting more then 5 letters like [-1,2] or [14,2]
		if (buff[5] != '\n') {
			check = 1;
			// read the extra letters
			read(0, buff, 1);
			while (buff[0] != '\n') {
				read(0, buff, 1);
			}
			write(1, NOT_FOUND_MES, strlen(NOT_FOUND_MES));

		// deal with getting 9 or 0 as a place
		} else if ((buff[1] == '0') || (buff[1] == '9') || (buff[3] == '0') ||
				   (buff[3] == '9')) {
			check = 1;
			write(1, NOT_FOUND_MES, strlen(NOT_FOUND_MES));

		// deal with full space
		} else if (player->board[row][column] != '-') {
			check = 1;
			write(1, INVALID_MES, strlen(INVALID_MES));

		// try to add the piece and deal with invalid space
		} else if (!Check_New_Piece(player, row, column, player->my_Piece,
									player->rival_Piece, Change_Add)) {
			check = 1;
			write(1, INVALID_MES, strlen(INVALID_MES));
		}
	}
	// insert the piece and update the shared memory
	player->board[row][column] = player->my_Piece;
	player->pieces_Amount++;

	output[0] = player->my_Piece;
	output[1] = buff[1];
	output[2] = buff[3];
	output[3] = '\0';
	strcpy(memory, output);

	return;
}


/******************************************************************************
* function name: Can_Continue.                                                *
* the Input: a Game_Assets pointer, the request and rival pieces as chars.	  *
* the output: 1 if we can continue(there a place for new piece, 0 otherwise.  *
* the Function operation: the function check if we can place a new piece on   *
*						  the board.										  *
******************************************************************************/
int Can_Continue(Game_Assets *player, char this_Piece, char other_Piece) {
	// check if the board is full
	if (player->pieces_Amount == (BOARD_SIZE * BOARD_SIZE))
		return 0;
	else {
		/**
		 * check if there a empty place we can put the request piece so it eat
		 * an other player piece (note we only checking not changing)
		 */
		int i, j;
		for (i = 0; i < BOARD_SIZE; i++) {
			for (j = 0; j < BOARD_SIZE; j++) {
				if ((player->board[i][j] == this_Piece) &&
					(Check_New_Piece(player, i, j, this_Piece,
									 other_Piece, Change_Check)))
					return 1;
			}
		}
		return 0;
	}
}


/******************************************************************************
* function name: End_Game_Mes.                                                *
* the Input: a Game_Assets pointer.                                     	  *
* the output: no output.                                                      *
* the Function operation: the function print the winner or tie				  *
******************************************************************************/
void End_Game_Mes(Game_Assets *player) {
	/**
	 * note that if the game stop in the middle their will be blank space so
	 * we need to check both white and black
	 */
	int i, j, whites = 0, blacks = 0;
	for (i = 0; i < BOARD_SIZE; i++) {
		for (j = 0; j < BOARD_SIZE; j++) {
			if (player->board[i][j] == 'w')
				whites++;
			else if (player->board[i][j] == 'b')
				blacks++;
		}
	}

	// print the result
	if (whites > blacks)
		write(1, WHITE_WIN_MES, strlen(WHITE_WIN_MES));
	else if (whites < blacks)
		write(1, BLACK_WIN_MES, strlen(BLACK_WIN_MES));
	else
		write(1, EVEN_MES, strlen(EVEN_MES));
}