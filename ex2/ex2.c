/******************************************
* Eran haberman
* 8923101
* ex2
******************************************/
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

int to_Stop = 0; // indicate if we need to stop the program


/******************************************************************************
* function name: Stop_Handle.                                                 *
* the Input: the signal that raise the function.                              *
* the output: no output.                                                      *
* the Function operation: the function change the to_Stop argument to 1 thus  *
*						  signal the program to stop.						  *
******************************************************************************/
void Stop_Handle(int sig);


int main(int argc, char *argv[]) {
	char buff[2] = "  ";
	// make the pipes and pids
	int infd[2], child_To_Child[2];
	pid_t child1, child2;

	// make the sigaction
	struct sigaction sig_Act;
	sigset_t block_Mask;
	sigfillset(&block_Mask);
	sig_Act.sa_mask = block_Mask;
	sig_Act.sa_flags = 0;

	//make the stop/SIGALRM signal
	sig_Act.sa_handler = Stop_Handle;
	if (sigaction(SIGALRM, &sig_Act, NULL) != 0) {
		write(2, "Error ex2 didnt create signal",
			  strlen("Error ex2 didnt create signal"));
		exit(-1);
	}

	// setting the alarm
	int number = 0, position = 0;
	while (argv[1][position] != '\0') {
		number = (number * 10) + (int)(argv[1][position] - '0');
		position++;
	}
	alarm(number);

	// set the pipes
	if (pipe(infd) < 0) {
		write(2, "Error did not create pipe\n",
			  strlen("Error did not create pipe\n"));
		exit(-1);
	}
	if (pipe(child_To_Child) < 0) {
		write(2, "Error did not create pipe\n",
			  strlen("Error did not create pipe\n"));
		exit(-1);
	}

	// set the children
	child1 = fork();
	if (child1 < 0) {
		write(2, "Error did not create fork\n",
			  strlen("Error did not create fork\n"));
		exit(-1);
	} else if (child1 == 0) {
		dup2(child_To_Child[0], 0);
		close(infd[0]);
		close(infd[1]);
		close(child_To_Child[0]);
		close(child_To_Child[1]);
		execlp("./ex2_inp.out", "./ex2_inp.out", NULL);
	}
	// get the length of the child1 pid
	int child1_Copy = child1; // so we can destroy in the process
	int check = 10;
	position = 1;
	while (child1_Copy / check) {
		check *= 10;
		position++;
	}

	// make string from the pid
	char child1_Pid[position + 1];
	child1_Pid[position] = '\0';
	for (position--; position >= 0; position--) {
		child1_Pid[position] = (child1_Copy % 10) + '0';
		child1_Copy /= 10;
	}

	child2 = fork();
	if (child2 < 0) {
		write(2, "Error did not create fork\n",
			  strlen("Error did not create fork\n"));
		kill(child1, SIGINT);
		exit(-1);
	} else if (child2 == 0){
		dup2(infd[0], 0);
		dup2(child_To_Child[1], 1);
		close(infd[0]);
		close(infd[1]);
		close(child_To_Child[0]);
		close(child_To_Child[1]);
		// we send the pid of the first child to the second child
		execlp("./ex2_upd.out", "./ex2_upd.out", child1_Pid, NULL);
	}

	// set ex2
	close(infd[0]);
	close(child_To_Child[0]);
	close(child_To_Child[1]);

	// loop until the alarm set on
	while (!to_Stop) {
		read(0, buff, 2);
		write(infd[1], buff, 2);
	}

	// end the program
	close(infd[1]);
	kill(child2, SIGINT);
	kill(child1, SIGINT);
	return 0;
}


/******************************************************************************
* function name: Stop_Handle.                                                 *
* the Input: the signal that raise the function.                              *
* the output: no output.                                                      *
* the Function operation: the function change the to_Stop argument to 1 thus  *
*						  signal the program to stop.						  *
******************************************************************************/
void Stop_Handle(int sig) {
	to_Stop = 1;
}