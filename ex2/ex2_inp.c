/******************************************
* Eran haberman
* 8923101
* ex2
******************************************/
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>

#define LINE_SIZE 4
#define MATRIX_SIZE (LINE_SIZE * LINE_SIZE)
#define NUMBER_SIZE 4 // 4 digit

int to_Stop = 0; // indicate if we need to stop the program
int to_Print = 0; // indicate if we need to print


/******************************************************************************
* function name: Stop_Handle.                                                 *
* the Input: the signal that raise the function.                              *
* the output: no output.                                                      *
* the Function operation: the function change the to_Stop argument to 1 thus  *
*						  signal the program to stop.						  *
******************************************************************************/
void Stop_Handle(int sig);


/******************************************************************************
* function name: Stop_Handle.                                                 *
* the Input: the signal that raise the function.                              *
* the output: no output.                                                      *
* the Function operation: the function change the to_Print argument to 1 thus *
*						  signal the program to print.						  *
******************************************************************************/
void Print_Handle(int sig);


/******************************************************************************
* function name: Init.                                                        *
* the Input: the board - array of chars.                                      *
* the output: no output.                                                      *
* the Function operation: the function Initialize the board to "0000"         *
******************************************************************************/
void Init(char board[MATRIX_SIZE][NUMBER_SIZE + 1]);


/******************************************************************************
* function name: Get_Board.                                                   *
* the Input: the board - array of chars.                                      *
* the output: no output.                                                      *
* the Function operation: the function get the numbers from STDIN and set the *
*						  board accordingly.				                  *
******************************************************************************/
void Get_Board(char board[MATRIX_SIZE][NUMBER_SIZE + 1]);


/******************************************************************************
* function name: Draw.                                                        *
* the Input: the board - array of chars.                                      *
* the output: no output.                                                      *
* the Function operation: the function draw the board in graphical shape.     *
******************************************************************************/
void Draw(char board[MATRIX_SIZE][NUMBER_SIZE + 1]);


int main(int argc, char *argv[]) {
	/**
	 * we use a square so we can use LINE_SIZE^2
	 * every string is 4 digit (NUMBER_SIZE) + '\0'
	 * as indicates by the game rules
	 */
	char board[MATRIX_SIZE][NUMBER_SIZE + 1];

	// make the sigaction
	struct sigaction sig_Act;
	sigset_t block_Mask;
	// block all signal except SIGINT
	sigfillset(&block_Mask);
	sigdelset(&block_Mask, SIGINT);
	sig_Act.sa_mask = block_Mask;
	sig_Act.sa_flags = 0;

	//make the print/SIGUSR1 signal
	sig_Act.sa_handler = Print_Handle;
	if (sigaction(SIGUSR1, &sig_Act, NULL) != 0) {
		write(2, "Error ex2_inp didnt create signal",
			  strlen("Error ex2_inp didnt create signal"));
		exit(-1);
	}

	//make the stop/SIGINT signal
	sig_Act.sa_handler = Stop_Handle;
	if (sigaction(SIGINT, &sig_Act, NULL) != 0) {
		write(2, "Error ex2_inp didnt create signal",
			  strlen("Error ex2_inp didnt create signal"));
		exit(-1);
	}

	// loop until we get SIGINT
	while (!to_Stop) {
		if (to_Print) {
			to_Print = 0;
			Init(board);
			Get_Board(board);
			if (!to_Stop)
				Draw(board);
		}
	}

	// end the program
	write(1, "BYE BYE\n", strlen("BYE BYE\n"));
	return 0;
}


/******************************************************************************
* function name: Stop_Handle.                                                 *
* the Input: the signal that raise the function.                              *
* the output: no output.                                                      *
* the Function operation: the function change the to_Stop argument to 1 thus  *
*						  signal the program to stop.						  *
******************************************************************************/
void Stop_Handle(int sig) {
	to_Stop = 1;
}


/******************************************************************************
* function name: Stop_Handle.                                                 *
* the Input: the signal that raise the function.                              *
* the output: no output.                                                      *
* the Function operation: the function change the to_Print argument to 1 thus *
*						  signal the program to print.						  *
******************************************************************************/
void Print_Handle(int sig) {
	to_Print = 1;
}


/******************************************************************************
* function name: Init.                                                        *
* the Input: the board - array of chars.                                      *
* the output: no output.                                                      *
* the Function operation: the function Initialize the board to "0000"         *
******************************************************************************/
void Init(char board[MATRIX_SIZE][NUMBER_SIZE + 1]) {
	int i, j;
	for (i = 0; i < (MATRIX_SIZE); i++) {
		for (j = 0; j < NUMBER_SIZE; j++) {
			board[i][j] = '0';
		}
		board[i][j] = '\0'; // end number string with '\0' to end the string
	}
}


/******************************************************************************
* function name: Get_Board.                                                   *
* the Input: the board - array of chars.                                      *
* the output: no output.                                                      *
* the Function operation: the function get the numbers from STDIN and set the *
*						  board accordingly.				                  *
******************************************************************************/
void Get_Board(char board[MATRIX_SIZE][NUMBER_SIZE + 1]) {
	// is fail help as make sure we always get 16 numbers
	int is_End, cur_Num_Size, place, is_Fail = 0;
	char buff[1], current_Num[NUMBER_SIZE + 1];
	do { // get the numbers form STDIN
		is_End = read(0, buff, 1);
		if (buff[0] == ',') { // check there is a number
			write(2, "MISSING NUMBER\n", strlen("MISSING NUMBER\n"));
			exit(-1);
		}
		cur_Num_Size = 0;
		do { // get the current number
			if (cur_Num_Size == 4) { // check that we got 4 digit number
				write(2, "NUMBER TOO BIG\n", strlen("NUMBER TOO BIG\n"));
				exit(-1);
			}
			current_Num[cur_Num_Size] = buff[0]; // add digit to cur number
			cur_Num_Size++;
			current_Num[cur_Num_Size] = '\0'; // end the current number
			is_End = read(0, buff, 1);
		} while ((is_End != 0) && (buff[0] != ',') && (buff[0] != '\n'));

		// set the current number in the board in the current space
		for (cur_Num_Size--, place = 3 ; cur_Num_Size >= 0;
			cur_Num_Size--, place--) {
			board[is_Fail][place] = current_Num[cur_Num_Size];
		}
		is_Fail++;
	} while ((is_End != 0) && (buff[0] != '\n'));

	if (is_Fail != (MATRIX_SIZE)) { // check that we got 16 numbers
		write(2, "DID NOT GET 16 NUMBERS\n", strlen("DID NOT GET 16 NUMBERS\n"));
		exit(-1);
	}
	return;
}


/******************************************************************************
* function name: Draw.                                                        *
* the Input: the board - array of chars.                                      *
* the output: no output.                                                      *
* the Function operation: the function draw the board in graphical shape.     *
******************************************************************************/
void Draw(char board[MATRIX_SIZE][NUMBER_SIZE + 1]) {
	int i, j;
	for (i = 0; i < LINE_SIZE; i++) {
		write(1, "| ", 2); // draw the first wall
		for (j = 0; j < LINE_SIZE; j++) {
			// (i*NUMBER_SIZE) == raw, j == column
			if (!strcmp(board[(i * NUMBER_SIZE) + j], "0000"))
				write(1, "    ", NUMBER_SIZE);
			else // number is not 0
				write(1, board[(i * NUMBER_SIZE) + j], NUMBER_SIZE);
			if (j == (LINE_SIZE - 1)) // check if the last number
				write(1, " |", 2); // draw the wall between numbers
			else
				write(1, " | ", 3); // draw the wall between numbers
		}
		write(1, "\n", 1);
	}
	write(1, "\n", 1);
	return;
}