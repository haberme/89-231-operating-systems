/******************************************
* Eran haberman
* 8923101
* ex2
******************************************/
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <malloc.h>
#include <signal.h>
#include <time.h>

#define LINE_SIZE 4
#define MATRIX_SIZE (LINE_SIZE * LINE_SIZE)
#define NUMBER_SIZE 4
#define SET_SLEEP ((rand() % 5) + 1)

typedef struct {
	int board[LINE_SIZE][LINE_SIZE];
	int number_Amount; // indicates the amount of number on the board
	char *pid;
} Game_Assets;

int to_Stop = 0; // indicate if we need to stop the program
int alarm_Off = 0; // indicate if we need to print


/******************************************************************************
* function name: Alarm_Handle.                                                *
* the Input: the signal that raise the function.                              *
* the output: no output.                                                      *
* the Function operation: the function change the alarm_Off argument to 1 thus *
*						  signal the program to print.						  *
******************************************************************************/
void Alarm_Handle(int sig);


/******************************************************************************
* function name: Stop_Handle.                                                 *
* the Input: the signal that raise the function.                              *
* the output: no output.                                                      *
* the Function operation: the function change the to_Stop argument to 1 thus  *
*						  signal the program to stop.						  *
******************************************************************************/
void Stop_Handle(int sig);


/******************************************************************************
* function name: Print_Board.                                                 *
* the Input: a Game_Assets pointer.                                     	  *
* the output: no output.                                              		  *
* the Function operation: the function print the board in one line to the 	  *
*						  STDOUT as "x1,x2,x3,...".	and send SIGUSR1 to 	  *
*						  ex2_inp.c.						  			 	  *
******************************************************************************/
void Print_Board(Game_Assets *player);


/******************************************************************************
* function name: Init.                                                        *
* the Input: a Game_Assets pointer.                                     	  *
* the output: random sleep time.                                              *
* the Function operation: the function Initialize the board to 0 except two   *
*						  which be 2, set the amount of numbers in the board  *
*						  to 2, print the board and send SIGUSR1 to ex2_inp   *
*						  and return sleep time.   	  					      *
******************************************************************************/
int Init(Game_Assets *player);


/******************************************************************************
* function name: Auto_Add_Two.                                                *
* the Input: a Game_Assets pointer.                                     	  *
* the output: random sleep time.                                              *
* the Function operation: the function add 2 to the board if there is open    *
*						  space and up the amount of number on the board by 1 *
*						  and return random sleep value						  *
******************************************************************************/
int Auto_Add_Two(Game_Assets *player);


/******************************************************************************
* function name: Add_Up.                                                      *
* the Input: a Game_Assets pointer, i and j positions of the from place and i *
*			 and j positions of the to place (we add the numbers in the to    *
*			 position).													      *
* the output: 1 if we add numbers 0 otherwise                                 *
* the Function operation: the function check if two number are equal. if so it*
*						  add them up to one number and decreasing the amount *
*						  of number on the board by 1.						  *
******************************************************************************/
int Add_Up(Game_Assets *player, int from_I, int from_J, int to_I, int to_J);


/******************************************************************************
* function name: Move_Up.                                                     *
* the Input: a Game_Assets pointer.                                     	  *
* the output: random sleep time.                                              *
* the Function operation: the function move the numbers up (adding them if    *
*						  they equal) and sending new sleep time.	  		  *
*						  the function also print the board and send SIGUSR2  *
*						  ex2_inp.c 										  *
******************************************************************************/
int Move_Up(Game_Assets *player);


/******************************************************************************
* function name: Move_Down.                                                   *
* the Input: a Game_Assets pointer.                                     	  *
* the output: random sleep time.                                              *
* the Function operation: the function move the numbers down (adding them if  *
*						  they equal) and sending new sleep time.	  		  *
*						  the function also print the board and send SIGUSR2  *
*						  ex2_inp.c 										  *
******************************************************************************/
int Move_Down(Game_Assets *player);


/******************************************************************************
* function name: Move_Right.                                                  *
* the Input: a Game_Assets pointer.                                     	  *
* the output: random sleep time.                                              *
* the Function operation: the function move the numbers to the right (adding  *
*						  them if they equal) and sending new sleep time.	  *
*						  the function also print the board and send SIGUSR2  *
*						  ex2_inp.c 										  *
******************************************************************************/
int Move_Right(Game_Assets *player);


/******************************************************************************
* function name: Move_Left.                                                   *
* the Input: a Game_Assets pointer.                                     	  *
* the output: random sleep time.                                              *
* the Function operation: the function move the numbers to the right (adding  *
*						  them if they equal) and sending new sleep time.	  *
*						  the function also print the board and send SIGUSR2  *
*						  ex2_inp.c 										  *
******************************************************************************/
int Move_Left(Game_Assets *player);


int main(int argc, char *argv[]) {
	int alarm_Set;
	Game_Assets *player = (Game_Assets*) malloc(sizeof(Game_Assets));
	player->pid = argv[1];
	srand(time(NULL)); // set the rand to random by clock

	// make the sigaction
	struct sigaction sig_Act;
	sigset_t block_Mask;
	// block all signal except SIGINT
	sigfillset(&block_Mask);
	sigdelset(&block_Mask, SIGINT);
	sig_Act.sa_mask = block_Mask;
	sig_Act.sa_flags = 0;

	//make the print/SIGALRM signal
	sig_Act.sa_handler = Alarm_Handle;
	if (sigaction(SIGALRM, &sig_Act, NULL) != 0) {
		write(2, "Error ex2_upd didnt create signal",
			  strlen("Error ex2_upd didnt create signal"));
		exit(-1);
	}

	//make the stop/SIGINT signal
	sig_Act.sa_handler = Stop_Handle;
	if (sigaction(SIGINT, &sig_Act, NULL) != 0) {
		write(2, "Error ex2_upd didnt create signal",
			  strlen("Error ex2_upd didnt create signal"));
		exit(-1);
	}

	alarm_Set = Init(player); // start the game
	alarm(alarm_Set);

	// loop until we get SIGINT
	char buff[2] = "  ";
	while (!to_Stop) {
		read(0, buff, 2);
		switch(buff[0]) { // get letter from user for movement
			case 'A': alarm_Set = Move_Left(player);
					  alarm(alarm_Set);
				break;
			case 'D': alarm_Set = Move_Right(player);
					  alarm(alarm_Set);
				break;
			case 'W': alarm_Set = Move_Up(player);
					  alarm(alarm_Set);
				break;
			case 'X': alarm_Set = Move_Down(player);
					  alarm(alarm_Set);
				break;
			case 'S': alarm_Set = Init(player);
					  alarm(alarm_Set);
				break;
			default : break;
		}

		if (alarm_Off) { // the alarm went off
			alarm_Off = 0;
			alarm_Set = Auto_Add_Two(player);
			alarm(alarm_Set);
			Print_Board(player);
		}

		// reset the buff
		buff[0] = ' ';
	}

	// end the program
	alarm(0); // end the alarm
	free(player);
	return 0;
}


/******************************************************************************
* function name: Alarm_Handle.                                                *
* the Input: the signal that raise the function.                              *
* the output: no output.                                                      *
* the Function operation: the function change the alarm_Off argument to 1 thus *
*						  signal the program to print.						  *
******************************************************************************/
void Alarm_Handle(int sig) {
	alarm_Off = 1;
}


/******************************************************************************
* function name: Stop_Handle.                                                 *
* the Input: the signal that raise the function.                              *
* the output: no output.                                                      *
* the Function operation: the function change the to_Stop argument to 1 thus  *
*						  signal the program to stop.						  *
******************************************************************************/
void Stop_Handle(int sig) {
	to_Stop = 1;
}


/******************************************************************************
* function name: Print_Board.                                                 *
* the Input: a Game_Assets pointer.                                     	  *
* the output: no output.                                              		  *
* the Function operation: the function print the board in one line to the 	  *
*						  STDOUT as "x1,x2,x3,...".	and send SIGUSR1 to 	  *
*						  ex2_inp.c.						  			 	  *
******************************************************************************/
void Print_Board(Game_Assets *player) {
	int i, j, place_Check, place, number_Int;
	char number_String[NUMBER_SIZE];
	for (i = 0; i < LINE_SIZE; i++) {
		for (j = 0; j < LINE_SIZE; j++) {
			number_Int = player->board[i][j];
			if ((i + j)) // print ","between numbers
				write(1, ",", 1);
			if (!number_Int) { // deal with number == 0
				write(1, "0", 1);
				continue;
			}
			// transform the number to string
			// place indicate the place of the right digit
			for (place = 1, place_Check = 10;; place++, place_Check *= 10) {
				if (!(number_Int / place_Check))
					break;
			}

			// build the string
			number_String[place] = '\0';
			for (place--; place > -1; place--) {
				number_String[place] = (number_Int % 10) + '0';
				number_Int /= 10;
			}

			write(1, number_String, strlen(number_String)); // write the number
		}
	}
	write(1, "\n", 1);

	// send SIGUSR1 to ex2_inp.c
	pid_t pid;
	pid = fork();
	if (pid < 0) {
		write(2, "Error did not create fork\n",
			  strlen("Error did not create fork\n"));
		exit(-1);
	} else if (pid == 0) {
		execlp("kill", "kill", "-SIGUSR1", player->pid, NULL);
	}
	return;
}


/******************************************************************************
* function name: Init.                                                        *
* the Input: a Game_Assets pointer.                                     	  *
* the output: random sleep time.                                              *
* the Function operation: the function Initialize the board to 0 except two   *
*						  which be 2, set the amount of numbers in the board  *
*						  to 2, print the board and send SIGUSR1 to ex2_inp   *
*						  and return sleep time.   	  					      *
******************************************************************************/
int Init(Game_Assets *player) {
	player->number_Amount = 0; // initialize the amount of number on the board
	int i, j, rand_Num;
	// set the all board to 0
	for (i = 0; i < LINE_SIZE; i++){
		for(j = 0; j < LINE_SIZE; j++){
			player->board[i][j] = 0;
		}
	}
	Auto_Add_Two(player); // add two to the board
	rand_Num = Auto_Add_Two(player); // add two to the board
	Print_Board(player); //print the board
	return rand_Num;
}


/******************************************************************************
* function name: Auto_Add_Two.                                                *
* the Input: a Game_Assets pointer.                                     	  *
* the output: random sleep time.                                              *
* the Function operation: the function add 2 to the board if there is open    *
*						  space and up the amount of number on the board by 1 *
*						  and return random sleep value						  *
******************************************************************************/
int Auto_Add_Two(Game_Assets *player) {
	if (player->number_Amount != 16) { //make sure we have empty spots
		// check will make sure we placed the two in empty spot
		int rand_Num, check = 0;
		while (!check) {
			rand_Num = (rand() % 16) + 1; // get the random place
			if (!player->board[rand_Num / LINE_SIZE][(rand_Num % LINE_SIZE) -
							   						 1]) {// == 0
				player->board[rand_Num / LINE_SIZE][(rand_Num % LINE_SIZE) -
							  					     1] = 2;
				check = 1;
			}
		}
		player->number_Amount++;
	}
	return SET_SLEEP;
}


/******************************************************************************
* function name: Add_Up.                                                      *
* the Input: a Game_Assets pointer, i and j positions of the from place and i *
*			 and j positions of the to place (we add the numbers in the to    *
*			 position).													      *
* the output: 1 if we add numbers 0 otherwise                                 *
* the Function operation: the function check if two number are equal. if so it*
*						  add them up to one number and decreasing the amount *
*						  of number on the board by 1.						  *
******************************************************************************/
int Add_Up(Game_Assets *player, int from_I, int from_J, int to_I, int to_J) {
	if ((0 <= to_I) && (to_I < LINE_SIZE) && // check the i position
		(0 <= to_J) && (to_J < LINE_SIZE) && // check the j position
		(player->board[from_I][from_J] == player->board[to_I][to_J])) {
		player->board[from_I][from_J] = 0;
		player->board[to_I][to_J] *= 2;
		player->number_Amount--;
		return 1;
	}
	return 0;
}


/******************************************************************************
* function name: Move_Up.                                                     *
* the Input: a Game_Assets pointer.                                     	  *
* the output: random sleep time.                                              *
* the Function operation: the function move the numbers up (adding them if    *
*						  they equal) and sending new sleep time.	  		  *
*						  the function also print the board and send SIGUSR2  *
*						  ex2_inp.c 										  *
******************************************************************************/
int Move_Up(Game_Assets *player) {
	// up_By = the number of places to move the current number up
	// is_Added indicates if we add number in previous moving
	int i, j, up_By, is_Added;
	for (j = 0; j < LINE_SIZE; j++) {
		is_Added = 0;
		for (i = 1; i < LINE_SIZE; i++) {
			up_By = 1;
			// find out by how much can move the current number
			while ((i - up_By >= 0) && (!player->board[i - up_By][j])) {
				up_By++;
			}
			up_By--;
			if (up_By) { // check if we need to move up at all;
				player->board[i - up_By][j] = player->board[i][j];
				player->board[i][j] = 0;
			}
			// check if we need to add up the numbers
			if (!is_Added)
				is_Added = Add_Up(player, i - up_By, j, i - up_By - 1, j);
			else
				is_Added = 0;
		}
	}

	// print the board
	Print_Board(player);
	return SET_SLEEP;
}


/******************************************************************************
* function name: Move_Down.                                                   *
* the Input: a Game_Assets pointer.                                     	  *
* the output: random sleep time.                                              *
* the Function operation: the function move the numbers down (adding them if  *
*						  they equal) and sending new sleep time.	  		  *
*						  the function also print the board and send SIGUSR2  *
*						  ex2_inp.c 										  *
******************************************************************************/
int Move_Down(Game_Assets *player) {
	// down_By = the number of places to move the current number down
	// is_Added indicates if we add number in previous moving
	int i, j, down_By, is_Added;
	for (j = 0; j < LINE_SIZE; j++) {
		is_Added = 0;
		for (i = (LINE_SIZE - 1); i >= 0; i--) {
			down_By = 1;
			// find out by how much can move the current number
			while ((i + down_By <= (LINE_SIZE - 1)) &&
				   (!player->board[i + down_By][j])) {
				down_By++;
			}
			down_By --;
			if (down_By) { // check if we need to move up at all;
				player->board[i + down_By][j] = player->board[i][j];
				player->board[i][j] = 0;
			}
			// check if we need to add up the numbers
			if (!is_Added)
				is_Added = Add_Up(player, i + down_By, j, i + down_By + 1, j);
			else
				is_Added = 0;
		}
	}

	// print the board
	Print_Board(player);
	return SET_SLEEP;
}


/******************************************************************************
* function name: Move_Right.                                                  *
* the Input: a Game_Assets pointer.                                     	  *
* the output: random sleep time.                                              *
* the Function operation: the function move the numbers to the right (adding  *
*						  them if they equal) and sending new sleep time.	  *
*						  the function also print the board and send SIGUSR2  *
*						  ex2_inp.c 										  *
******************************************************************************/
int Move_Right(Game_Assets *player) {
	// right_By = the number of places to move the current number right
	// is_Added indicates if we add number in previous moving
	int i, j, right_By, is_Added;
	for (i = 0; i < LINE_SIZE; i++) {
		is_Added = 0;
		for (j = (LINE_SIZE - 1); j >= 0; j--) {
			right_By = 1;
			// find out by how much can move the current number
			while ((j + right_By <= (LINE_SIZE - 1)) &&
				   (!player->board[i][j + right_By])) {
				right_By++;
			}
			right_By--;
			if (right_By) { // check for number movement
				player->board[i][j + right_By] = player->board[i][j];
				player->board[i][j] = 0;
			}
			// check if we need to add up the numbers

			if (!is_Added)
				is_Added = Add_Up(player, i, j + right_By, i, j + right_By +1);
			else
				is_Added = 0;
		}
	}

	// print the board
	Print_Board(player);
	return SET_SLEEP;
}


/******************************************************************************
* function name: Move_Left.                                                   *
* the Input: a Game_Assets pointer.                                     	  *
* the output: random sleep time.                                              *
* the Function operation: the function move the numbers to the left (adding   *
*						  them if they equal) and sending new sleep time.	  *
*						  the function also print the board and send SIGUSR2  *
*						  ex2_inp.c 										  *
******************************************************************************/
int Move_Left(Game_Assets *player) {
	// left_By = the number of places to move the current number left
	// is_Added indicates if we add number in previous moving
	int i, j, left_By, is_Added;
	for (i = 0; i < LINE_SIZE; i++) {
		is_Added = 0;
		for (j = 1; j < LINE_SIZE; j++) {
			left_By = 1;
			// find out by how much can move the current number
			while ((j - left_By >= 0) &&
				   (!player->board[i][j - left_By])) {
				left_By++;
			}
			left_By--;
			if (left_By) { // check for number movement
				player->board[i][j - left_By] = player->board[i][j];
				player->board[i][j] = 0;
			}
			// check if we need to add up the numbers

			if (!is_Added)
				is_Added = Add_Up(player, i, j - left_By, i, j - left_By - 1);
			else
				is_Added = 0;
		}
	}

	// print the board
	Print_Board(player);
	return SET_SLEEP;
}