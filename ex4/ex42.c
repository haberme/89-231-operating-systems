/******************************************
* Eran haberman
* 8923101
* ex4 - ex42.c
******************************************/
#include <unistd.h>
#include <stdlib.h>
#include <sys/fcntl.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/shm.h>

#define SHM_SIZE 1 // the shared memory size
#define FILE_NAME "result.txt"

union semun {
	int val;
	struct semid_ds *buf;
	ushort *array;
};

int internal_count = 0;


void Write_Int(int num, int fd);


void Write_Count(int fd, int thread);


int main(int argc, char *argv[]) {
	// initial the shared memory
	key_t key;
	int shmid;
	char *memory;

	// create the file result.txt
	int resultfd;
	if ((resultfd = open(FILE_NAME, O_CREAT|O_TRUNC|O_WRONLY, 0666)) <
	    0) {
		write(2, "had a problem creating the result file\n",
			  strlen("had a problem creating the result file\n"));
		exit(-1);
	}

	// create the shared memory
	// create the  key
	if ((key = ftok(FILE_NAME, 'e')) == -1) {
		write(2, "ftok had a problem\n", strlen("ftok had a problem\n"));
		exit(-1);
	}

	// create the memory
	if ((shmid = shmget(key, SHM_SIZE, 0666 | IPC_CREAT)) == -1) {
		write(2, "shmget had a problem crearing the memory\n",
			  strlen("shmget had a problem crearing the memory\n"));
		exit(-1);
	}

	// attach to the segment to get a pointer to it
	memory = shmat(shmid, NULL, 0);
	if (memory == (char *)(-1)) {
		write(2, "shmat had a problem attaching to the memory\n",
			  strlen("shmat had a problem attaching to the memory\n"));
		exit(-1);
	}

	// main logic
	while ((memory[0] != '7') && (memory[0] != '8')) {
		if (memory[0] > '0') {
			switch (memory[0]) {
				case '6': Write_Count(resultfd, 6);
						  break;
				case '7': Write_Count(resultfd, 7);
						  break;
				case '8': Write_Count(resultfd, 8);
						  break;
				// deal with 1-5
				default : internal_count += (int)(memory[0] - '0');
			}
		}
		//////////
		sleep(2);
		//////////
	}

	// delete the shared memory
	// detach from the segment
	if (shmdt(memory) == -1) {
		write(2, "shmdt had a problem detaching to the memory\n",
			  strlen("shmdt had a problem detaching to the memory\n"));
		exit(-1);
	}

	// remove the memory
	if (shmctl(shmid, IPC_RMID, NULL) == -1) {
		write(2, "shmctl had a problem removing the memory\n",
			  strlen("shmctl had a problem removing the memory\n"));
		exit(-1);
	}
	return 0;
}


void Write_Int(int num, int fd) {
	if (num == 0)
		return;
	else {
		char cur;
		cur = (num % 10) + '0';
		num = num / 10;
		Write_Int(num, fd);
		write(fd, &cur, 1);
	}
	return;
}


void Write_Count(int fd, int thread) {
	Write_Int(thread, fd);
	write(fd, ",", 1);
	if (internal_count == 0)
		write(fd, "0", 1);
	else
		Write_Int(internal_count, fd);
	write(fd, "\n", 1);
	return;
}