/******************************************
* Eran haberman
* 8923101
* ex4 - ex41.c
******************************************/
#include <unistd.h>
#include <stdlib.h>
#include <sys/fcntl.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/shm.h>

#define SHM_SIZE 1 // the shared memory size
#define FILE_NAME "result.txt"


int main(int argc, char *argv[]) {
	char buff[2];

	// initial the shared memory
	key_t key;
	int shmid;
	char *memory;

	// get and attach to the shared memory
	// create key
	if ((key = ftok(FILE_NAME, 'e')) == -1) {
		write(2, "ftok had a problem\n", strlen("ftok had a problem\n"));
		exit(-1);
	}

	// create memory
	if ((shmid = shmget(key, SHM_SIZE, 0)) == -1) {
		write(2, "shmget had a problem crearing the memory\n",
			  strlen("shmget had a problem crearing the memory\n"));
		exit(-1);
	}

	// attach to the segment to get a pointer to it
	memory = shmat(shmid, NULL, 0);
	if (memory == (char *)(-1)) {
		write(2, "shmat had a problem attaching to the memory\n",
			  strlen("shmat had a problem attaching to the memory\n"));
		exit(-1);
	}

	// main logic
	write(1, "Please enter request code\n",
		  strlen("Please enter request code\n"));
	read(0, buff, 2);
	while (!((buff[0] == '9') && (buff[1] == '\n'))) {
		if (buff[1] != '\n') {
			while (buff[0] != '\n') {
				read(0, buff,1);
			}
		} else if ((buff[0] > '0') && (buff[0] < '9')){
			memory[0] = buff[0];
		}

		write(1, "Please enter request code\n",
			  strlen("Please enter request code\n"));
		read(0, buff, 2);
	}

	// detach from the segment
	if (shmdt(memory) == -1) {
		write(2, "shmdt had a problem detaching to the memory\n",
			  strlen("shmdt had a problem detaching to the memory\n"));
		exit(-1);
	}
	return 0;
}